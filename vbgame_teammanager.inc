<?php
/**
 * @file
 * Part of vbgame Module
 * This inc provides the Teammanager functionality
 */

/**
 * Implements hook_form().
 *
 * Teammanager
 *
 * This form is used to display, add and edit the team database.
 */
function vbgame_teammanager_form() {
  $form = array();
  $form['updatexml_top'] = array(
    '#type' => 'submit',
    '#value' => 'Update XML Team',
    '#submit' => array('vbgame_teammanager_form_updatexml_submit'),
    '#validate' => array('vbgame_teammanager_form_updatexml_validate'),
  );
  $form['table'] = vbgame_teammanager_form_tabelle();
  $form['updatexml_bottom'] = array(
    '#type' => 'submit',
    '#value' => 'Update XML Team',
    '#submit' => array('vbgame_teammanager_form_updatexml_submit'),
    '#validate' => array('vbgame_teammanager_form_updatexml_validate'),
  );
  return $form;
}
/**
 * This function generates the team table.
 */
function vbgame_teammanager_form_tabelle() {

  $resource = db_query("SELECT * FROM {vbgame_teamtable} ORDER BY jahr DESC, name ASC");

  $rs = array();
  while ($row = db_fetch_object($resource)) {
    $rs[] = $row;
  }

  $header = array(
    'teamid' => t('teamID'),
    'name' => t('display name'),
    'pattern' => t('pattern'),
    'sid' => t('match series id'),
    'jahr' => t('season'),
    'weight' => t('weight'),
    'backend' => t('backend'),
    'rankingtype' => t('ranking type'),
    'timestamp' => t('xml timestamp'),
    'remoteid' => t('remote teamid'),
  );

  $options = array();
  foreach ($rs as $team) {
    $options[$team->teamid] = array(
      'teamid' => $team->teamid,
      'name' => check_plain($team->name),
      'pattern' => check_plain($team->pattern),
      'sid' => check_plain($team->sid),
      'jahr' => check_plain($team->jahr),
      'weight' => check_plain($team->weight),
      'backend' => check_plain($team->backend),
      'rankingtype' => check_plain($team->rankingtype),
      'timestamp' => check_plain($team->xmlfetchtime),
      'remoteid' => check_plain($team->remoteID),
    );
  }
  // Using <tableselect> from elements module.
  $table = array(
    '#type' => 'tableselect',
    '#multiple' => FALSE,
    '#empty' => t('There are no teams in the database.'),
    '#header' => $header,
    '#options' => $options,
  );
  return $table;
}

/**
 * Multi step wizard to add a team to the database.
 */
function vbgame_teammanager_add_form($form_state) {
  if (!$form_state['storage']['step']) {
    $form_state['storage']['step'] = 1;
  }
  $form_state['values'] = $form_state['storage']['values'];
  drupal_set_title(t('Add Team Step @step of @totalsteps', array(
    '@step' => $form_state['storage']['step'],
    '@totalsteps' => 3,
  )));

  $step = $form_state['storage']['step'];

  $form = array();

  switch ($step) {
    case 3:
      $teamlist = vbgame_service_query_team_list(
        $form_state['values']['backend'],
        $form_state['values']['saison'],
        $form_state['values']['staffel']);

      $form['team'] = array(
        '#title' => 'Select Team',
        '#type' => 'select',
        '#options' => $teamlist,
        '#default_value' => $form_state['values']['team'],
        '#weight' => 3,
        '#required' => TRUE,
      );

    case 2:
      $staffellist = vbgame_service_query_staffel_list(
        $form_state['values']['backend'],
        $form_state['values']['saison']);

      $form['staffel'] = array(
        '#title' => 'Staffel',
        '#type' => 'select',
        '#options' => $staffellist,
        '#default_value' => $form_state['values']['staffel'],
        '#element_validate' => array('vbgame_teammanager_add_form_element_validate'),
        '#weight' => 2,
        '#required' => TRUE,
        '#disabled' => ($step > 2),
      );

    case 1:
      $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Team Name'),
        '#default_value' => $form_state['values']['name'],
        '#size' => 25,
        '#maxlength' => 200,
        '#description' => t("Team Display Name"),
        '#weight' => -5,
        '#element_validate' => array('vbgame_teammanager_add_form_element_validate'),
        '#required' => TRUE,
      );

      $form['backend'] = array(
        '#title' => 'Backend',
        '#type' => 'select',
        '#default_value' => $form_state['values']['backend'],
        '#options' => vbgame_get_options('backends_enabled'),
        '#size' => 1,
        '#weight' => -1,
        '#required' => TRUE,
        '#disabled' => ($step > 1),
      );

      $form['saison'] = array(
        '#type' => 'select',
        '#title' => t('Season'),
        '#default_value' => $form_state['values']['saison'],
        '#options' => vbgame_get_options('season'),
        '#size' => 1,
        '#weight' => 1,
        '#element_validate' => array('vbgame_teammanager_add_form_element_validate'),
        '#required' => TRUE,
        '#disabled' => ($step > 1),
      );
  }
  // Corresponds to 'edit-back' in form_submit
  $form['back'] = array(
    '#value' => 'back',
    '#type' => 'submit',
    '#weight' => 6,
    '#disabled' => ($step == 1),
  );
  // Corresponds to 'edit-next' in form_submit.
  $form['next'] = array(
    '#type' => 'submit',
    '#value' => ($step > 2) ? 'finish' : 'next',
    '#weight' => 7,
  );

  return $form;
}

/**
 * Validate function for add-wizard.
 */
function vbgame_teammanager_add_form_validate($form, &$form_state) {
  if (!$form_state['storage']['values']) {
    $form_state['storage']['values'] = array();
  }
  $values = $form_state['storage']['values'] + $form_state['values'];
  $step = $form_state['storage']['step'];

  $pattern = $values['team'];
  $sid = $values['staffel'];
  $season = $values['saison'];
  $name = $values['name'];

  // Warn if season is not "current season".
  if ($step >= 1 AND variable_get('vbgame_saison', 0000) != $season) {
    $message = t('Current default season is @season. You will not get automatic (cron) updates for this team!',
      array('@season' => variable_get('vbgame_saison', 0000)));
    drupal_set_message($message, 'warning');
  }

  // Team Display Name should be unique.
  if ($step >= 1 AND !vbgame_db_check_name_unique($name, $season)) {
    form_set_error('name', t('Team name not unique in this season!'));
    return;
  }

  // Pattern should be unique per match series!
  if ($step >= 3 AND !vbgame_db_check_pattern_unique($pattern, $season, $sid)) {
    form_set_error('team', t('Team already defined in database!'));
    return;
  }
}

/**
 * Validate elements from team-manager form.
 */
function vbgame_teammanager_add_form_element_validate($element, &$form_state) {
  $service_list = variable_get('vbgame_service_list', array());

  switch ($element['#name']) {
    case 'staffel':
      $service_name = $form_state['values']['backend'];
      $service_module = $service_list[$service_name]['module'];

      $ok = call_user_func(
        implode("_", array($service_module, 'validate_form_element_staffel')),
        $service_name,
        $element['#value']
      );
      if (!$ok) {
        form_error($element, t('Please select a valid match series!'));
      }
      break;
  }
}

/**
 * Form submit function for multistep team add wizard.
 */
function vbgame_teammanager_add_form_submit(&$form, &$form_state) {
  // Multistep processing.
  if (!$form_state['storage']['values']) {
    $form_state['storage']['values'] = array();
  }
  // Increment the button, overwrite the storage values with the new values,
  // and return.
  if ($form_state['clicked_button']['#id'] == 'edit-back') {
    $form_state['storage']['step']--;
    $form_state['storage']['values'] = $form_state['values'] + $form_state['storage']['values'];
  }
  elseif ($form_state['clicked_button']['#id'] == 'edit-next') {
    $form_state['storage']['step']++;
    $form_state['storage']['values'] = $form_state['values'] + $form_state['storage']['values'];
  }

  if ($form_state['storage']['step'] > 3) {
    drupal_set_message(t('Team Added'));
    $form['#redirect'] = 'vbgame/tm';

    // If this is the last step,
    // clear the storage and rebuild so that the form doesn't rebuild,
    // and the #redirect works.
    $values = $form_state['storage']['values'] + $form_state['values'];

    $object = new stdClass();
    $object->name = check_plain($values['name']);
    $object->pattern = check_plain(trim($values['team']));
    $object->sid = check_plain($values['staffel']);
    $object->jahr = check_plain($values['saison']);
    $object->backend = check_plain($values['backend']);
    $object->rankingtype = 'threepoint';
    $object->remoteID = NULL;
    $object->weight = 1;

    $ret = drupal_write_record('vbgame_teamtable', $object);

    if ($ret == SAVED_NEW) {
      drupal_set_message(t('Team successfully added to Database. Use manual update to fetch initial content!'));
    }
    else {
      form_set_error('', t('Team INSERT failed!'));
    }

    $form_state['rebuild'] = TRUE;
    unset($form_state['storage']);
  }
}

/**
 * This form lets you edit a selected Team.
 */
function vbgame_teammanager_edit_form($form_state) {
  // Start a wizard like form.
  if (!$form_state['storage']['step']) {
    $form_state['storage']['step'] = 1;
  }
  $form_state['values'] = $form_state['storage']['values'];
  drupal_set_title(t('Edit Team Step @step of @totalsteps', array(
    '@step' => $form_state['storage']['step'],
    '@totalsteps' => 2,
  )));

  $step = $form_state['storage']['step'];

  $form = array();

  switch ($step) {
    case 2:
      // STEP 2: Show editable fields.
      $teaminfo = vbgame_team_info($form_state['values']['teamid']);

      $form['season'] = array(
        '#type' => 'value',
        '#value' => $teaminfo->jahr);

      $form['name'] = array(
        '#type' => 'textfield',
        '#title' => 'Team display name',
        '#description' => t("Team display name is a custom team name."),
        '#default_value' => $teaminfo->name,
        '#size' => 25,
        '#maxlength' => 200,
        '#required' => TRUE);

      $form['rankingtype'] = array(
        '#type' => 'select',
        '#title' => 'Ranking Type',
        '#description' => t("Ranking method for this match series."),
        '#options' => vbgame_get_options('rankingtype'),
        '#default_value' => $teaminfo->rankingtype,
        '#required' => TRUE);

    case 1:
      // STEP 1: Select a Team (LIST).
      $form['teamid'] = array(
        '#title' => 'Select Team',
        '#type' => 'select',
        '#options' => vbgame_get_options('form_team_list'),
        '#default_value' => $form_state['values']['teamid'],
        '#weight' => -2,
        '#required' => TRUE,
        '#disabled' => ($step > 1),
      );
  }
  // Corresponds to 'edit-next' in form_submit.
  $form['next'] = array(
    '#type' => 'submit',
    '#value' => ($step > 1) ? 'finish' : 'next',
    '#weight' => 7,
  );

  return $form;
}

/**
 * This form lets you delete a selected Team.
 */
function vbgame_teammanager_delete_form() {
  $form = array();

  $form['teamid'] = array(
    '#title' => 'Select Team',
    '#type' => 'select',
    '#options' => vbgame_get_options('form_team_list'),
    '#default_value' => -1,
    '#weight' => -2,
    '#required' => TRUE,
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => 'Delete Team',
    '#submit' => array('vbgame_teammanager_delete_form_submit'),
  );
  return $form;
}

/**
 * Validate Team select.
 *
 * checks, if you have selected a team.
 */
function vbgame_teammanager_form_updatexml_validate($form, &$form_state) {
  if (empty($form['table']['#value'])) {
    form_set_error('table', t('Select a Team!'));
    return;
  }
}

/**
 * Team delete Function.
 */
function vbgame_teammanager_delete_form_submit($form, &$form_state) {
  $teamid = $form['teamid']['#value'];
  // Remove Content from Database.
  vbgame_db_delete_team_from('vbgame_tabelle', $teamid);
  vbgame_db_delete_team_from('vbgame_spielplan', $teamid);
  // Remove Teamtable entry.
  db_query("DELETE FROM {vbgame_teamtable} WHERE teamid = %d", $teamid);
  if (db_affected_rows() == 1) {
    drupal_set_message(t('Team DELETE successful!'));
  }
  else {
    form_set_error('', t('Team DELETE failed!'));
  }
}

/**
 * Team delete validate.
 */
function vbgame_teammanager_delete_form_validate($form, &$form_state) {
  $teamid = $form['teamid']['#value'];
  if (vbgame_db_check_teamid_is_associated($teamid)) {
    form_set_error('teamid', t('Team is associated to a teampage!'));
    return;
  }
}

/**
 * Implements form_edit_validate().
 *
 * Teammanager
 */
function vbgame_teammanager_edit_form_validate($form, &$form_state) {
  // Nothing to Validate on first step!
  if ($form_state['storage']['step'] <= 1) {
    return TRUE;
  }

  // Team Display Name should be unique
  // (not really a requirement, but it does not make much sense to me).
  $season = $form_state['values']['season'];
  $name = $form_state['values']['name'];

  if (!vbgame_db_check_name_unique($name, $season)) {
    form_set_error('name', t('Team Name not unique in this season!'));
    return;
  }
}
/**
 * Implements form_edit_submit().
 *
 * Teammanager
 */
function vbgame_teammanager_edit_form_submit($form, &$form_state) {
  // Multistep processing.
  if (!$form_state['storage']['values']) {
    $form_state['storage']['values'] = array();
  }
  // Increment the button, overwrite the storage values with the new values,
  // and return.
  if ($form_state['clicked_button']['#id'] == 'edit-back') {
    $form_state['storage']['step']--;
    $form_state['storage']['values'] = $form_state['values'] + $form_state['storage']['values'];
  }
  elseif ($form_state['clicked_button']['#id'] == 'edit-next') {
    $form_state['storage']['step']++;
    $form_state['storage']['values'] = $form_state['values'] + $form_state['storage']['values'];
  }

  // Tell Drupal we are redrawing the same form.
  $form_state['rebuild'] = TRUE;

  // Finalize on last step.
  if ($form_state['storage']['step'] > 2) {
    // If this is the last step,
    // clear the storage and rebuild so that the form doesn't rebuild,
    // and the #redirect works.
    $form_state['#redirect'] = 'vbgame/tm';
    $values = $form_state['storage']['values'] + $form_state['values'];
    unset($form_state['storage']);
    drupal_set_message(t('Update Team Info.'));

    // UPDATE query.
    db_query("UPDATE {vbgame_teamtable} SET name = '%s', rankingtype = '%s' WHERE teamid = %d",
      check_plain($values['name']),
      check_plain($values['rankingtype']),
      check_plain($values['teamid'])
    );

    if (db_affected_rows() == 1) {
      drupal_set_message(t('Team UPDATE successful!'));
    }
    else {
      form_set_error('', t('Team UPDATE failed!'));
    }

    return $form_state['#redirect'];
  }
}
/**
 * Implements form_updatexml_submit().
 *
 * Teammanager
 */
function vbgame_teammanager_form_updatexml_submit($form, &$form_state) {
  drupal_set_message(t('Team XML UPDATE called!'));
  $teamid = $form['table']['#value'];
  vbgame_team_update_all($teamid, TRUE, NULL);
}

/**
 * Check, if pattern is unique this match series.
 */
function vbgame_db_check_pattern_unique($pattern, $season, $sid) {
  $count = db_result(db_query("SELECT COUNT(*) FROM {vbgame_teamtable} t WHERE t.pattern = '%s' AND t.jahr = '%s' AND t.sid = '%s'",
    $pattern,
    $season,
    $sid));
  return ($count == 0);
}

/**
 * Check if Name is unique in specified season.
 */
function vbgame_db_check_name_unique($name, $season) {
  $count = db_result(db_query("SELECT COUNT(*) FROM {vbgame_teamtable} t WHERE t.name = '%s' AND t.jahr = '%s'", $name, $season));
  return ($count == 0);
}

/**
 * Check if Team is associated to any of our nodes.
 */
function vbgame_db_check_teamid_is_associated($teamid) {
  $count = db_result(db_query("SELECT COUNT(*) FROM {vbgame_teampage} tp WHERE tp.teamid = %d", $teamid));
  return ($count > 0);
}
