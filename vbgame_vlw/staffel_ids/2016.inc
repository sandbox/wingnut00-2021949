$aktive = array(
  "Aktive" => "---------- AKTIVE ----------",
  "382" => "Regionalliga Sued, Herren",
  "623" => "Regionalliga Sued, Damen",
  "384" => "Oberliga, Herren",
  "325" => "Oberliga, Damen",
  "313" => "Landesliga Nord, Herren",
  "312" => "Landesliga Sued, Herren",
  "326" => "Landesliga Nord, Damen",
  "327" => "Landesliga Sued, Damen",
  "Aktive Nord" => "---------- AKTIVE NORD----------",
  "315" => "Bezirksliga Nord, Herren",
  "322" => "Bezirksliga Nord, Damen",
  "360" => "A-Klasse 1 Nord, Herren",
  "328" => "A-Klasse 1 Nord, Damen",
  "329" => "A-Klasse 2 Nord, Damen",
  "362" => "B-Klasse 1 Nord, Herren",
  "321" => "B-Klasse 2 Nord, Herren",
  "330" => "B-Klasse 1 Nord, Damen",
  "331" => "B-Klasse 2 Nord, Damen",
  "332" => "B-Klasse 3 Nord, Damen",
  "Aktive Ost" => "---------- AKTIVE OST----------",
  "365" => "Bezirksliga Ost, Herren",
  "334" => "Bezirksliga Ost, Damen",
  "366" => "A-Klasse 1 Ost, Herren",
  "367" => "A-Klasse 2 Ost, Herren",
  "335" => "A-Klasse 1 Ost, Damen",
  "336" => "A-Klasse 2 Ost, Damen",
  "368" => "B-Klasse 1 Ost, Herren",
  "589" => "B-Klasse 2 Ost, Herren",
  "337" => "B-Klasse 1 Ost, Damen",
  "338" => "B-Klasse 2 Ost, Damen",
  "339" => "B-Klasse 3 Ost, Damen",
  "340" => "B-Klasse 4 Ost, Damen",
  "323" => "B-Klasse 5 Ost, Damen",
  "Aktive Sued" => "---------- AKTIVE SUED----------",
  "317" => "Bezirksliga Sued, Herren",
  "344" => "Bezirksliga Sued, Damen",
  "373" => "A-Klasse 1 Sued, Herren",
  "365" => "A-Klasse 1 Sued, Damen",
  "346" => "A-Klasse 2 Sued, Damen",
  "445" => "B-Klasse 1 Sued, Herren",
  "347" => "B-Klasse 1 Sued, Damen",
  "348" => "B-Klasse 2 Sued, Damen",
  "Aktive West" => "---------- AKTIVE WEST----------",
  "375" => "Bezirksliga West, Herren",
  "351" => "Bezirksliga West, Damen",
  "376" => "A-Klasse 1 West, Herren",
  "377" => "A-Klasse 2 West, Herren",
  "352" => "A-Klasse 1 West, Damen",
  "353" => "A-Klasse 2 West, Damen",
  "378" => "B-Klasse 1 West, Herren",
  "354" => "B-Klasse 1 West, Damen",
  "447" => "B-Klasse 2 West, Damen",
  "355" => "B-Klasse 3 West, Damen",
);
$jugend = array(
  "Jugend" => "---------- JUGEND ----------",
  "453" => "Leistungsstaffel U20 maennlich Nord",
  "454" => "Leistungsstaffel U20 maennlich Sued",
  "476" => "Leistungsstaffel U20 weiblich Nord",
  "477" => "Leistungsstaffel U20 weiblich Sued",
  "548" => "Leistungsstaffel U18 maennlich Nord",
  "478" => "Leistungsstaffel U18 weiblich Nord",
  "479" => "Leistungsstaffel U18 weiblich Sued",
  "Jugend Nord" => "---------- JUGEND NORD ----------",
  "452" => "Bezirksstaffel U20 Nord 1 maennlich",
  "461" => "Bezirksstaffel U18 Nord 1 maennlich",
  "480" => "Bezirksstaffel U20 Nord 1 weiblich",
  "481" => "Bezirksstaffel U20 Nord 2 weiblich",
  "482" => "Bezirksstaffel U18 Nord 1 weiblich",
  "485" => "Bezirksstaffel U16 Nord 1 weiblich",
  "Jugend Ost" => "---------- JUGEND OST ----------",
  "540" => "Bezirksstaffel U20 Ost 1 maennlich",
  "541" => "Bezirksstaffel U20 Ost 2 maennlich",
  "463" => "Bezirksstaffel U18 Ost 1 maennlich",
  "593" => "Bezirksstaffel U16 Ost 1 maennlich",
  "625" => "Bezirksstaffel U16 Ost 2 maennlich",
  "487" => "Bezirksstaffel U20 Ost 1 weiblich",
  "488" => "Bezirksstaffel U20 Ost 2 weiblich",
  "489" => "Bezirksstaffel U20 Ost 3 weiblich",
  "490" => "Bezirksstaffel U20 Ost 4 weiblich",
  "324" => "Bezirksstaffel U20 Ost 5 weiblich",
  "492" => "Bezirksstaffel U18 Ost 1 weiblich",
  "493" => "Bezirksstaffel U18 Ost 2 weiblich",
  "494" => "Bezirksstaffel U18 Ost 3 weiblich",
  "497" => "Bezirksstaffel U16 Ost 1 weiblich",
  "590" => "Bezirksstaffel U16 Ost 2 weiblich",
  "Jugend Sued" => "---------- JUGEND SUED ----------",
  "459" => "Bezirksstaffel U20 Sued 1 maennlich",
  "465" => "Bezirksstaffel U18 Sued 2 maennlich",
  "473" => "Bezirksstaffel U16 Sued 1 maennlich",
  "500" => "Bezirksstaffel U20 Sued 1 weiblich",
  "501" => "Bezirksstaffel U20 Sued 2 weiblich",
  "502" => "Bezirksstaffel U18 Sued 1 weiblich",
  "505" => "Bezirksstaffel U16 Sued 1 weiblich",
  "627" => "Bezirksstaffel U16 Sued 2 weiblich",
  "Jugend West" => "---------- JUGEND WEST ----------",
  "458" => "Bezirksstaffel U20 West 1 maennlich",
  "629" => "Bezirksstaffel U18 West 1 maennlich",
  "508" => "Bezirksstaffel U20 West 1 weiblich",
  "509" => "Bezirksstaffel U20 West 2 weiblich",
  "608" => "Bezirksstaffel U20 West 3 weiblich",
  "511" => "Bezirksstaffel U18 West 1 weiblich",
  "513" => "Bezirksstaffel U16 West 1 weiblich",
);
